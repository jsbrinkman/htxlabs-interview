using System;

// Not a typo, this is meant to be in the same namespace
namespace HtxQuestionThree
{
    interface IParticleGroupOrchestratorTest
    {
        void createParticleGroupReturnsGroup();

        void createParticleGroupReturnsUniqueGroupsWhenCalledMultiplesTimes();

        void createParticleGroupWithInvalidIdThrowsException();

        void attachParticleGroupToModelAttachesParticleGroup();

        void attachParticleGroupToModelWithInvalidModelThrowsException();

        void attachParticleGroupToModelUpdatesState();

        // Because an object attached to a model or rendered in the world is a full 3D entity
        // I'm allowing for transitioning between those two states
        void attachParticleGroupToModelWithParticleGroupRenderedInWorldUpdatesState();

        // An object rendered in screenspace doesn't actually exist in the world
        // To prevent accidentally placing client-side objects in a shared space, I'm preventing
        // screenspace ParticleGroups from being attached or placed
        void attachParticleGroupToModelWithParticleGroupRenderedInScreenSpaceThrowsException();

        void placeParticleGroupInWorldPlacesParticleGroup();

        void placeParticleGroupInWorldWithParticleGroupAttachedToModelUpdatesState();

        void placeParticleGroupInWorldWithInvalidWorldThrowsException();

        void placeParticleGroupInWorldWithInvalidParticleGroupThrowsException();

        void placeParticleGroupInWorldWithParticleGroupRenderedInScreenSpaceThrowsException();

        void renderParticleGroupInScreenSpaceRendersParticleGroup();

        void renderParticleGroupInScreenSpaceWithInvalidParametersThrowsException();

        void renderParticleGroupInScreenSpaceWithParticleGroupAttachedToModelThrowsException();

        void renderParticleGroupInScreenSpaceWithParticleGroupPlacedInWorldThrowsException();

        void scaleParticleGroupSizeScalesCorrectly();

        void scaleParticleGroupSizeWithInvalidPercentageThrowsException();

        void deleteParticleGroupDeletsParticleGroup();

        void deleteParticleGroupOnAlreadyDeletedParticleGroupThrowsException();

        void deleteParticleGroupOnNonexistentParticleGroupThrowsException();
    
}