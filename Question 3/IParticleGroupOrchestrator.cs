using System;

namespace HtxQuestionThree
{
    //I'm working off the assumption that the ParticleGroup object contains some data to define their placement, size, state, etc.
    //which requires passing in the actual object so it can be modified directly.
    //If ParticleGroups are stored in some external data source, then passing in a ParticleGroup Id would be sufficient
    interface IParticleGroupOrchestrator
    {
        // I'm assuming we don't keep the particle group lookup table stored in memory
        // requiring a DB lookup in order to fetch the appropriate information
        Task<ParticleGroup> createParticleGroup(string typeId);

        void attachParticleGroupToModel(ParticleGroup particleGroup, Model model);

        //WorldReference is the data we use to represent the world we're placing the ParticleGroups in
        //CoordinateSet is being used to represent however the engine handles coordinates
        void placeParticleGroupInWorld(ParticleGroup particleGroup, WorldReference worldReference, CoordinateSet worldCoordinates);

        void renderParticleGroupInScreenSpace(ParticleGroup particleGroup, CoordinateSet screenLocation);

        void scaleParticleGroupSize(ParticleGroup particleGroup, float scalePercentage);

        void deleteParticleGroup(ParticleGroup particleGroup);        
    }
    
}