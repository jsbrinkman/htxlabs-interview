using System;

namespace HtxQuestionTwo
{
    class SquareCounter {
        public long countSquares(long N) {
            if (N <= 0) {
                throw new Exception("The grid size cannot be smaller than 1x1!");
            }

            return recursiveCountSquares(N);
        }

        private long recursiveCountSquares(long N) {
            if (N == 1) {
                return 1;
            } else {
                return N*N + recursiveCountSquares(N-1);
            }
        } 
    }    
}