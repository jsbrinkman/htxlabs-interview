# Question 1

Given a point P defined by the coordinates (P<sub>x</sub>, P<sub>y</sub>, P<sub>z</sub>) and a sphere S with a radius of S<sub>r</sub>, with an origin S<sub>o</sub> at (S<sub>x</sub>, S<sub>y</sub>, S<sub>z</sub>) we can say the point P is in S if the distance between P and S<sub>o</sub> is less than P<sub>r</sub>. The easiest way to calculate this is using the pythagorean theorem, modified for a 3D object; that formula would be:

(P<sub>x</sub> - S<sub>x</sub>)<sup>2</sup> + (P<sub>y</sub> - S<sub>y</sub>)<sup>2</sup> + (P<sub>z</sub> - S<sub>z</sub>)<sup>2</sup> <  P<sub>r</sub><sup>2</sup>