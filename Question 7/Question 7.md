# Question 7

What I'm describing is my general troubleshooting process for DB issues. When confronted with a problem like that, I'll go through these steps in roughly this order--stopping if one of them finds the problem.

## Steps to Diagnose
0. Check error logs. This is always the first step, a defensively designed codebase should be providing good logs and stack traces for any errors that're occurring. If the requests are being caused by a bug, this'll catch it.
1. Check existing SRE monitoring surrounding the DB. If our logging and monitoring is configured correctly, it should have database usage metrics.
2. Check monitoring surrounding the APIs. If the DB is suddenly being overloaded, that could be caused by the APIs being hit with an abnormally high number of requests.
3. Check deployment history. If the issue just started happening, check what changed in the most recent deployment. It's possible a bit of internal code is causing it.
4. If we're not hosting our own DBs check if the host recently released an update or a patch
5. Cross my fingers and google.

## Short Term Fixes
These are the fixes I'd attempt to implement if the situation is absolutely critical, something which we *need* to deal with immediately.
1. If the issue was caused by a recent deployment, roll back that deployment.
2. Scale up the DB. Sometimes scaling up the database can serve as a bandaid solution, buying us time to implement a real fix.
3. If the issue is being caused by a specific service or API, add rate limiting. If the situation is truly dire, take that service offline.

## Long Term Fixes
These are the programming techniques which help to prevent this issue from occurring to begin with.
1. Use batching whenever possible. If the system needs to retrieve 100 records, don't make 100 calls, build a query which can fetch all 100 records in a single go.
2. Optimize the queries which are most frequently used; this can mean sticking to querying by indices in SQL-likes, or avoiding cross-partition queries in something like CosmosDB
3. Optimize the DB itself by creating indices which accurately reflect the most common use cases.
4. Avoid chatty designs in general. Try to minimize the number of DB calls being made across the entire application. 
