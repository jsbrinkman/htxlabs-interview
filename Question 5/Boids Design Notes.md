# Boids Design Notes

## Defining a Gnat Swarm
* The average bird can really only fly forward, they'll turn while doing it, but the forward moment is constant. Thus, a flock will always be facing in roughly the same direction. Insects aren't restricted in the same way, allowing them to effectively have no real facing.
* Lack of concern for facing. Bird flight is defined by sweeping turns and straight paths, since that's the most energy efficient method. Insects don't care about efficiency in the same way, their flight is very jittery with lots of rapid turns and reversals.
* Purpose of the swarm. Birds flock in order to travel, insects swarm primarily when drawn to food. This means a bird flock is relatively mobile, while the gnat swarm is more or less stationary, with individuals having tight and irregular orbits around a single point.

## Demonstration
*[Video Demonstration](https://www.awesomescreenshot.com/video/6823006?key=be737a1325ef31558ffd23c014fd882a)*

You can also run the simulation by cloning the repo, and running index.html from any modern browser.
