//Canvas size
let width = 250;
let height = 250

// Number of boids initially placed in the world
const numBoids = 250;

// Distance in which boids will attract each other
const visualRange = 100;

// Distance before boids will begin to repel each other
const minDistance = 25;

// How much boids in visual range attract each other
const attractionConstant = 0.01

// How much boids within minDistance repel each other
const repulsionConstant = 0.05

// How much boids within a swarm want to match each others' speed
const velocityMatchingConstant = 0.03;

// Chance of deviating from the given orbitRadius
const wobbleChance = 0.5

// The percent by which a given boid may deviate from its orbit radius
const wobbleConstant = 0.3

var boids = [];

function initializeSimulation() {
    for (var i = 0; i < numBoids; i += 1) {
        boids.push({
            id: i,
            x: Math.random() * width,
            y: Math.random() * height,
            dx: Math.random() * 20,
            dy: Math.random() * 20,

            // Randomize orbit radius in a range of minDistance to visualRange - minDistance
            orbitRadius: Math.floor(Math.random() * (visualRange - 2*minDistance) + minDistance),

            // Randomize tick angle to simulate the speed of circular motion
            angleTick: Math.floor(Math.random() * (7.5 - 1) + 1),
            
            // Current angle, the boid always assumes wherever it starts is 0 degrees
            // this is acceptable because we're using roughly circular orbits
            orbitalAngle: 0,

            // Randomly determine which direction the boid is going to orbit
            invertOrbit: Math.random() < 0.5,
        });
    }
}

// Find all boids within a certain range of a given boid
function findAllBoidsInRange(boid, range = visualRange) {
    let boidSystem = []

    for (let neighbor of boids) {
        if (distance(boid, neighbor) <= range && neighbor.id != boid.id) {
            boidSystem.push(neighbor)
        }
    }
    return boidSystem 
}

// Adds the tell-tale circular motion that gnat swarms are known for
function calculateOrbit(boid) {
    const boidSystem = findAllBoidsInRange(boid)

    // Only perform orbital calculations if there's at least one neighoring boid
    // and only *sometimes* perform an orbit
    if (boidSystem.length > 0 && Math.random() < 0.33) {
        [boidCenterX, boidCenterY] = calculateSwarmCenterOfMass(boidSystem)
        
        // Randomize change of wobbling
        if (Math.random() < wobbleChance) {

            // Randomize wobble direction
            if (Math.random() < 0.5) {
                boid.x -= boid.orbitRadius * wobbleConstant
            } else {
                boid.y += boid.orbitRadius * wobbleConstant
            }
        }

        // Formula for rotation of a point about another point in space
        boid.x = (Math.cos(Math.PI * 2 * boid.orbitalAngle / 360)*(boid.x - boidCenterX) - Math.sin(Math.PI * 2 * boid.orbitalAngle / 360)*( boid.y - boidCenterY) + boidCenterX)
        boid.y = (Math.sin(Math.PI * 2 * boid.orbitalAngle / 360)*(boid.x - boidCenterX) + Math.cos(Math.PI * 2 * boid.orbitalAngle / 360)*( boid.y - boidCenterY) + boidCenterY)

        // This is a hack. If I was really going to do this right, I'd have tick angle be a function of dx and dy
        // But for this demo, a constant tick angle is sufficient
        if (boid.invertOrbit) {
            boid.orbitalAngle -= boid.angleTick
        } else {
            boid.orbitalAngle += boid.angleTick
        }
    }
}

// Calculate center of the swarm
function calculateSwarmCenterOfMass(boidSystem) {   
    boidCenterX = boidSystem.map(thisBoid => thisBoid.x).reduce((a, b) => a + b, 0) / boidSystem.length
    boidCenterY = boidSystem.map(thisBoid => thisBoid.y).reduce((a, b) => a + b, 0) / boidSystem.length
    return [boidCenterX, boidCenterY]
}

// Calculate overall direction vector of the swarm
function calculateSwarmDirectionVector(boidSystem) {   
    boidVelocityX = boidSystem.map(thisBoid => thisBoid.dx).reduce((a, b) => a + b, 0) / boidSystem.length
    boidVelocityY = boidSystem.map(thisBoid => thisBoid.dy).reduce((a, b) => a + b, 0) / boidSystem.length
    return [boidVelocityX, boidVelocityY]
}

// Called initially and whenever the window resizes to update the canvas
// size and width/height variables.
function sizeCanvas() {
  const canvas = document.getElementById("boids");
  width = window.innerWidth;
  height = window.innerHeight;
  canvas.width = width;
  canvas.height = height;
}

// Find the center of mass of the other boids and adjust velocity slightly to
// point towards the center of mass based on how much boids attract each other.
function swarm(boid) {
  const boidSystem = findAllBoidsInRange(boid)

  if (boidSystem.length > 0) {
    [boidCenterX, boidCenterY] = calculateSwarmCenterOfMass(boidSystem)
    boid.dx += (boidCenterX - boid.x) * attractionConstant;
    boid.dy += (boidCenterY - boid.y) * attractionConstant;
  }
}

// Move away from other boids that are too close to avoid colliding
function avoidOthers(boid) {
    const boidSystem = findAllBoidsInRange(boid, minDistance)
    let directionX = 0;
    let directionY = 0;

    for (let neighbor of boidSystem) {
        directionX += boid.x - neighbor.x;
        directionY += boid.y - neighbor.y;
    }
    boid.dx += directionX * repulsionConstant;
    boid.dy += directionY * repulsionConstant;
}

// Find the average velocity (speed and direction) of the other boids and
// adjust velocity slightly to match.
function matchVelocity(boid) {
    const boidSystem = findAllBoidsInRange(boid)

    if (boidSystem.length > 0) {
        [swarmVelocityX, swarmVelocitY] = calculateSwarmDirectionVector(boidSystem)
        boid.dx += (swarmVelocityX - boid.dx) * velocityMatchingConstant;
        boid.dy += (swarmVelocitY - boid.dy) * velocityMatchingConstant;
    }
}

// Iteration loop
function drawAnimationFrame() {
    // Update each boid
    for (let boid of boids) {
        swarm(boid);
        avoidOthers(boid);
        matchVelocity(boid);
        limitSpeed(boid);

        // Set new position based on velocity
        boid.x += boid.dx;
        boid.y += boid.dy;
    }

    //Secondary loop to handle orbiting, as the orbits are a function of center of mass
    for (let boid of boids) {
        // Alter current position based on potential orbit about swarm center of mass
        calculateOrbit(boid);

        // Ensure the boid remains within the bounds
        keepWithinBounds(boid);
    }

    // Clear the canvas and redraw all the boids in their current positions
    const ctx = document.getElementById("boids").getContext("2d");
    ctx.clearRect(0, 0, width, height);
    for (let boid of boids) {
        drawBoid(ctx, boid);
    }

    // Schedule the next frame
    window.requestAnimationFrame(drawAnimationFrame);
}

// Basic functions pulled from internet
function drawBoid(ctx, boid) {
    ctx.fillStyle = "#FF0000";
    ctx.beginPath();
    ctx.moveTo(boid.x, boid.y);
    ctx.arc(boid.x, boid.y, 3, 0, 2 * Math.PI, false)
    ctx.fill();
    ctx.setTransform(1, 0, 0, 1, boid.dx, boid.dy);
}

// Constrain a boid to within the window. If it gets too close to an edge,
// nudge it back in and reverse its direction.
function keepWithinBounds(boid) {
    const margin = 200;
    const turnFactor = 3;
  
    if (boid.x < margin) {
      boid.dx += turnFactor;
    }
    if (boid.x > width - margin) {
      boid.dx -= turnFactor
    }
    if (boid.y < margin) {
      boid.dy += turnFactor;
    }
    if (boid.y > height - margin) {
      boid.dy -= turnFactor;
    }
}

function distance(boid1, boid2) {
    return Math.sqrt(
      (boid1.x - boid2.x) * (boid1.x - boid2.x) +
        (boid1.y - boid2.y) * (boid1.y - boid2.y),
    );
}

function limitSpeed(boid) {
    const speedLimit = 20;
  
    const speed = Math.sqrt(boid.dx * boid.dx + boid.dy * boid.dy);
    if (speed > speedLimit) {
      boid.dx = (boid.dx / speed) * speedLimit;
      boid.dy = (boid.dy / speed) * speedLimit;
    }
}

window.onload = () => {
    window.addEventListener("resize", sizeCanvas, false);
    sizeCanvas();
    initializeSimulation();
    window.requestAnimationFrame(drawAnimationFrame);
  };