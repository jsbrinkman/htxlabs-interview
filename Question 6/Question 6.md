# Question 6

Minimum viable product for a web store is one where users are able to find and purchase items, which are then shipped to them.
* Account Creation - If the site intends to serve up targeted advertisements, there needs to be a method for tracking purchases and tying them to a person. Username and password are all that's necessary for this on the front end.
* Product Search - The user needs to be able to find their desired products. Whether this is a freetext search or just navigating menus doesn't matter, as long as the user can find what they're looking for without too much effort, it meets MVP.
* Inventory Display - The system needs to display the products which are available for sale. This doesn't need to be updated live for MVP, if the price changes or the item goes out of stock, the user will find out on refresh. This screen will also contain the 'purchase' button, which opens a new modal/window when the user clicks on it.
* Checkout Window - The checkout window needs to have space for the user to enter their payment information, shipping information, and discount codes (if applicable)
* Payment Options - The user needs to be able to pay for their goods using whatever payment options the product team dictates. For MVP the system only needs to support one type of payment.
* Shipping Information - The user needs to be able to specify where to ship their goods as part of completing their order
* Receipt Display - Following the order being placed, we need to display an itemized receipt to the user, which they can save. Not MVP if we're fine not doing business in California
* Advertisement Platform - We need a way to aggregate the user's purchase data and use that to recommend products.


## Notable Exclusions from MVP
* A Cart System - The users can purchase items directly from the product page. It's not *good* design, but cart systems can introduce additional complexity; because a cart requires the system to hold inventory in reserve to ensure the order can be fulfilled. If inventory can be reserved, there also needs to be a system to invalidate carts/items which've been inactive for a set time. If the system doesn't include this feature, it'd be possible for a botnet to put every piece of inventory into their carts, then sit on them forever.
* User Editable Profile - Some sites allow users to customize their profile, to save payment information, addresses, etc. While those systems are convenient, they're not necessary for the site to function. Users will need to type their information in every time they make an order.
* Emailed Order Confirmation - Emailing a recipt or confirmation to the user isn't necessary for the site to function; as long as the information is shown to them, it's enough.
* Order History - The data is already present in the system, but MVP doesn't require displaying it to the user